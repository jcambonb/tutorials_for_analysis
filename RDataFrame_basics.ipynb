{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# RDataFrame basics\n",
    "\n",
    "## A clase $\\texttt{RDataFrame}$ para a lectura de datos\n",
    "\n",
    "#### Iván Cambón Bouzas\n",
    "#### IFT-QCD + LFU group \n",
    "#### Instituto Galego de Física de Altas Enerxías (IGFAE)\n",
    "#### Universidade e Santiago de Compostela\n",
    "\n",
    "##### Repositorio de gitlab: <https://gitlab.cern.ch/jcambonb/tutorials_for_analysis>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ROOT\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## $\\texttt{RDataFrame}$: unha solución necesaria\n",
    "Documentación e métodos: <https://root.cern/doc/master/classROOT_1_1RDataFrame.html>\n",
    "\n",
    "Como vimos, a clase ``TTree`` é un gran éxito de ROOT a hora de almacenar e tratar datos. O problema reside en que, para os estándares actuales, a forma que temos de manipulalo está algo desfasada. Esto débese á dependencia dos bucles para poder obter e manipular a súa información. En C++ está moi ben xa que temos unha linguaxe compilada onde os bucles son rápidos. Non obstante, no cambio que estamos tendo cara a análise exclusiva en python, esta opción non é, para nada, a máis óptima tendo en conta os tamaños dos sets de datos cos que traballamos.\n",
    "\n",
    "Ante isto, fai uns poucos anos desenvolveuse a clase `RDataFrame`. A idea é sinxela, emular os `pandas.DataFrame` pero en ROOT. A clave reside en que todas as operacións que queramos facer co noso set de datos faránse internamente no `RDataFrame` (igual que os `pandas.DataFrame`). Esto optimiza moito os tempos, xa que en python non teremos que depender do bucle `for`  e teremos un bucle interno propio de `RDataFrame`. Ademáis, esta clase é compatible con __Multithreading__, polo que podemos mellorar aínda máis o rendimiento.\n",
    "\n",
    "Por último, como experiencia persoal, cando empecei no TFG aínda non estaba implementado `RDataFrame`. Tiña un set de datos moi grande, polo que para facer un simple plot dun histograma con ``TTree`` taríamos falando de 20-30 min de execución (para logo ter que cambiar algo da lenda). Cando apareciu e o aprendín a usalo, ver que o mesmo código tardaba como moito 5 min foi marabilloso. Dito isto, podemos comezar co tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Crear un `RDataFrame` a partir dun ``TTree``"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para crealo non é necesario abrir un `TFile`. Con saber a localización do .root file e o nome do ``TTree`` que ten dentro, e tan simple como"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path = \"./root_files/\"\n",
    "file = \"tree.root\"\n",
    "tree_name = \"tree\"\n",
    "\n",
    "\"\"\"\n",
    "ROOT.RDataFrame(tree_name, root_file)\n",
    "\"\"\"\n",
    "tdf = ROOT.RDataFrame(tree_name, path+file)\n",
    "\n",
    "tdf.Describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como vemos, o `RDataFrame` creouse sen problemas co `TTree` do tutorial anterior. Co método `Describe()` podemos saber as columnas que temos e o status do *Event loop*, que é bucle interno do que falabamos. Feito isto poderíamos introducir xa os métodos con este `RDataFrame`. Non obstante, para ter un exemplo máis realista, imos usar nTuplas reais de análise. Para iso imos considerar de novo a nTupla de MC $D_{s}^{*+} \\to D_s^+ \\gamma$ pero tamén imos usar unha nTupla de datos reais do ano 2018. En ambas, imos a coller or ``TTree`` que está en `DsGammaTuple`\n",
    "\n",
    "_*Nota*_: Recomendo correr esto nos nodos do IGFAE onde estes arquivos de datos están gardados no directorio `\"/scratch42/ivan.cambon/DsJ_Spectroscopy/\"`. Si non, pódense descargar con `scp` en local, pero non o recomendo ya que son arquivos bastante pesados."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_data = \"/scratch42/ivan.cambon/DsJ_Spectroscopy/Data/MagDown/2018/\"\n",
    "path_MC = \"/scratch42/ivan.cambon/DsJ_Spectroscopy/MC/Private/\"\n",
    "\n",
    "file_data = \"DsJ_Data_MagD18_500.root\"\n",
    "file_MC = \"DsstDsGamma_MC16_MagUp_Private.root\"\n",
    "dtt = \"DsGammaTuple\"\n",
    "\n",
    "tdf_data = ROOT.RDataFrame(dtt+\"/DecayTree\", path_data+file_data)\n",
    "tdf_MC = ROOT.RDataFrame(dtt+\"/DecayTree\", path_MC+file_MC)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tdf_data.Describe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tdf_MC.Describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Onde comprobamos que moitas columnas por cada nTupla. Cumpre destacar que o nome da nTupla de datos `\"DsJ_Data_MagD18_500.root\"` é porque neste directorio temos da orde de 1000 nTuplas de datos (de ahí que o set de datos sexa grande). Se queremos leer máis destas nTuplas podemos facer o seguinte\n",
    "\n",
    "1. Crear un set de python onde poñemos as nTuplas que queiramos  (todas teñen que ter o mesmo nome no ``TTree`` que queiramos leer)\n",
    "```python\n",
    "\n",
    "nTuplas = {path+\"nTuple1\", path+\"nTuple2\", path+\"nTuple3\"}\n",
    "tdf_data = ROOT.RDataFrame(tree_name, nTuplas)\n",
    "```\n",
    "\n",
    "2. Se todos os arquivos teñen un nome común (por exemplo as miñas nTuplas son `DsJ_Data_MagD18_num.root` sendo `\"num\"` un número do 0 ó 1000 e pico), podemos facer\n",
    "\n",
    "```python\n",
    "file_data = \"DsJ_Data_MagD18_*.root\"\n",
    "tdf_data = ROOT.RDataFrame(dtt+\"/DecayTree\", path_data+file_data)\n",
    "```\n",
    "\n",
    "Deste xeito estaríamos collendo todas as nTuplas que temos no directorio `\"path_data\"` que teñan o prefixo `\"DsJ_Data_MagD18_\"`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Métodos básicos"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imos comezar explicando métodos básicos que ten esta clase. Primeiro, para ver as filas do `RDataFrame` temos `Display` "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dis_MC = tdf_MC.Display()\n",
    "\n",
    "dis_MC.Print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Se queremos ver só columnas en específico e un número maior de filas facemos o seguinte."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Primeiro argumento: un set co nome das columnas\n",
    "Segundo argumento: o número de filas que queremos ver\n",
    "\"\"\"\n",
    "dis_MC_2 = tdf_MC.Display({\"eventNumber\", \"Ds_M\", \"gamma_PT\"}, 10) \n",
    "\n",
    "dis_MC_2.Print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para contar o número de candidatos (filas) que ten o `RDataFrame` usamos o método `Count()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MC_events = tdf_MC.Count().GetValue()  # O método GetValue é para ter o resultado como un valor\n",
    "Data_events = tdf_data.Count().GetValue() \n",
    "\n",
    "print(\"MC candidates = {0}\".format(MC_events))\n",
    "print(\"Data candidates = {0}\".format(Data_events))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "De igual forma temos métodos como `Mean()` ou `Max()` que nos permiten obter a media ou o máximo dunha variable dada. O seu uso sería equivalente ó de `Count()` pero non son tan útiles, polo que non nos pararemos moito neles\n",
    "\n",
    "Agora ben, para poder definir unha columna nova no `RDataFrame` que se calcule con outras usamos o método `Define()`. Para exemplificar isto, imos crear unha columna en ambos `RDataFrames` que será a masa desconvolucionada:\n",
    "\n",
    "$$M(D_s^+\\gamma) = m(D_s^+\\gamma)-m(K^+K^-\\pi^+)+m_{\\mathrm{PDG}}(D_s^+)$$\n",
    "\n",
    "Nas nTuplas, $m(D_s^+\\gamma)$ correspóndese ca columna `Dsg_M`, $m(K^+K^-\\pi^+)$ con `Ds_M` e $m_{\\mathrm{PDG}}(D_s^+) \\sim 1969 \\text{ } \\mathrm{MeV/c^2}$ (todas as variables cinemáticas destas nTuplas estan en MeV). Polo tanto, a definición faríase así:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\" \n",
    "Define(new_var_name, C++ expresión)\n",
    "\"\"\"\n",
    "\n",
    "tdf_MC = tdf_MC.Define(\"DsgM\", \"Dsg_M-Ds_M+1969\")\n",
    "tdf_data = tdf_data.Define(\"DsgM\", \"Dsg_M-Ds_M+1969\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tdf_MC.Describe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tdf_data.Describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vemos que se creou unha nova variable no `RDataFrame`. É necesario destacar que as expresións que podemos poñer no `RDataFrame` teñen que ser cd C++. Iso implica que non podemos meterlle nin variables predefinidas en python, nin operacións que veñen de python nin nada. Ante isto, temos que ter enconta que:\n",
    "\n",
    "- Para facer un raiz usamos `sqrt()`\n",
    "- Para facer funcións trigonométricas `cos()`, `sen()` e así\n",
    "- Para elevar o cadrado hai que facer `Ds_PX*Ds_PX`\n",
    "I\n",
    "sto é moi rudimentario e se queremos facer un cálculo máis complexo (facer boosts de Lorentz ou asi), complícase bastante. O que se pode facer é definir unha macro de C++ e logo compilala no código de python para así usala no `RDataFrame`. Para exemplicalo, vou usar unha macro chamada $\\texttt{Kinematics.C}$ que ten unha función que permite calcular a masa invariante:\n",
    "\n",
    "```C\n",
    "/*\n",
    "Function that calculates the invariant mass of a given 4-momentum\n",
    "-px: double. x component of 4-momentum\n",
    "-py: double. y component of 4-momentum\n",
    "-pz: double. z component of 4-momentum\n",
    "-pe: double. Energy of 4-momentum\n",
    "*/\n",
    "double mass(double px, double py, double pz, double pe)\n",
    "{\n",
    "    TLorentzVector P;\n",
    "    P.SetPx(px); P.SetPy(py); P.SetPz(pz) ; P.SetE(pe);\n",
    "\n",
    "    double mass = P.M();\n",
    "\n",
    "    return mass;\n",
    "}\n",
    "\n",
    "```\n",
    "\n",
    "Para compilala e usala facemos o seguinte\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ROOT.gROOT.ProcessLine(\".L ./C++_macros/Kinematics.C\") \n",
    "\n",
    "# Calculamos a masa invariante de dous kaons no RDataFrame de MC\n",
    "\n",
    "tdf_MC = tdf_MC.Define(\"KKM\", \"mass(Kpl_PX+Kmi_PX, Kpl_PY+Kmi_PY, Kpl_PZ+Kmi_PZ, Kpl_PE+Kmi_PE)\")\n",
    "tdf_MC.Describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Onde comprobamos que, efectivamente, esta variable definiouse sen ningun problema. Isto das macros si que se pode considerar algo máis avanzado. Pero compre destacar que pode ser moi útil para calcular variables novas. Outro método útil é `Snapshot()`, que nos permite gardar o `RDataFrame` como un `TTree` dentro dun .root file. Por defecto garda todas as columnas, pero podemos especificar que garde so as que nos interesan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_file = \"reduced_tree.root\"\n",
    "new_tree = \"DsGammaTree\"\n",
    "\n",
    "variables = {\"DsgM\", \"Ds_M\", \"gamma_PT\", \"eventNumber\", \"nVeloTracks\"}\n",
    "\n",
    "tdf_data.Snapshot(new_tree, \"./root_files/\"+new_file, variables)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "De novo, para introducir as variables que queremos temos que usar un set de python. Esto pode ser moi útil se temos sets muy grandes de datos e unha vez teñamos as variables que necesitemos, podemos gardar un `TTree` reducido que será máis sinxelo de tratar. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " Por último, se queremos activar o multithreading, temos que executar nunha celda a seguinte liña\n",
    "```python\n",
    "ROOT.EnableImplicitMT()\n",
    "```\n",
    "Se temos un equipo que o permita, o rendimiento debería subir bastante. Non obstante, ten o seus inconvintes xa que moitas veces perdemos a orde das filas que tiña o `RDataFrame`, polo que para certos cálculos pode ser peligroso."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Crear histogramas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Algo moi bo que ten `RDataFrame` é que podes definir directamente un histograma 1D mediante o método `Histo1D` e un 2D con `Histo2D`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "Histo1D((name, name, bins, xmin, xmax), xcolumn)\n",
    "Histo2D((name, name, bins, xmin, xmax, bins, ymin, ymax), xcolumn, ycolumn)\n",
    "\"\"\"\n",
    "\n",
    "MC_Dsg_mass_histo = tdf_MC.Histo1D((\"\", \"\", 100, 2000, 2700), \"DsgM\")\n",
    "Data_Dsg_mass_histo = tdf_data.Histo1D((\"\", \"\", 100, 2000, 2700), \"DsgM\")\n",
    "\n",
    "# Se temos unha columna que sexa un weight, podemos weightear o histograma como \n",
    "# tdf.Histo1D((\"\", \"\", 100, 2000, 2700), \"var\", \"weight\")\n",
    "\n",
    "Data_gamma_PT_CL_histo2D = tdf_data.Histo2D((\"\", \"\", 100, 0, 8000, 100, 0.1, 1.1), \"gamma_PT\", \"gamma_CL\")\n",
    "\n",
    "type(MC_Dsg_mass_histo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comprobamos que estes obxetos creados son un punteiro que apunta a un obxeto `TH1D` e un `TH2D`. Neste punto, sobre estes obxetos podemos directamente utilizar os métodos utilizados no tutorial __ROOT_basics_2__ para os histogramas, tanto para propiedades estadísticas como para facer plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c1 = ROOT.TCanvas()\n",
    "MC_Dsg_mass_histo.Draw()\n",
    "c1.Draw()\n",
    "\n",
    "c2 = ROOT.TCanvas()\n",
    "Data_Dsg_mass_histo.GetXaxis().SetTitle(\"Ds gamma mass\")\n",
    "Data_Dsg_mass_histo.SetLineColor(1)\n",
    "Data_Dsg_mass_histo.Draw()\n",
    "c2.Draw()\n",
    "\n",
    "c3 = ROOT.TCanvas()\n",
    "Data_gamma_PT_CL_histo2D.GetXaxis().SetTitle(\"gamma PT\")\n",
    "Data_gamma_PT_CL_histo2D.GetYaxis().SetTitle(\"gamma CL\")\n",
    "Data_gamma_PT_CL_histo2D.Draw(\"COLZ\")\n",
    "c3.Draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compre destacar que para certas operacións que vimos no tutorial anterior, vainos pasar algo como isto"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Data_Dsg_mass_histo_2 = Data_Dsg_mass_histo.Clone()\n",
    "Data_Dsg_mass_histo_2.Divide(MC_Dsg_mass_histo)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Esto pasa porque o obxecto que devolve o método `Histo1D` é un punteiro a un obxeto `TH1D`, non o obxeto `TH1D`. Entón, no caso que teñamos un histograma creado con `RDataFrame` e queiramos operar con el ou usalo como argumento doutra clase (poñelo nun `TLegend` por exemplo), teremos que usar o método auxiliar `GetPtr()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Data_Dsg_mass_histo_2 = Data_Dsg_mass_histo.Clone()\n",
    "Data_Dsg_mass_histo_2.Add(MC_Dsg_mass_histo.GetPtr())\n",
    "\n",
    "c2 = ROOT.TCanvas()\n",
    "lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)\n",
    "Data_Dsg_mass_histo_2.SetLineColor(2)\n",
    "Data_Dsg_mass_histo.SetLineColor(3)\n",
    "lgd.AddEntry(Data_Dsg_mass_histo.GetPtr(), \"Data\", \"l\")\n",
    "lgd.AddEntry(Data_Dsg_mass_histo_2, \"Data and MC addition\", \"l\") # A suma de histogramas que fixemos ahora si que é un TH1D, de ahí que non usemos aquí o GetPtr()\n",
    "Data_Dsg_mass_histo_2.Draw(\"same\")\n",
    "Data_Dsg_mass_histo.Draw(\"same\")\n",
    "lgd.Draw()\n",
    "c2.Draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Facer unha selección"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como vimos, nalgúns métodos de `TTree` podíamos impoñer condicións nas variables para reducir así o número de filas do noso obxeto. No caso de `RDataFrame` esto faise pero dunha maneira moito máis optimizada mediante o método `Filter()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tdf_MC_wind = tdf_MC.Filter(\"DsgM < 2400\")\n",
    "tdf_data_wind = tdf_data.Filter(\"DsgM < 2400\")\n",
    "\n",
    "print(\"MC candidates after cut = {0}\".format(tdf_MC_wind.Count().GetValue()))\n",
    "print(\"MC candidates after cut = {0}\".format(tdf_data_wind.Count().GetValue()))\n",
    "\n",
    "MC_Dsg_mass_win_histo = tdf_MC_wind.Histo1D((\"\", \"\", 100, 2000, 2700), \"DsgM\")\n",
    "Data_Dsg_mass_win_histo = tdf_data_wind.Histo1D((\"\", \"\", 100, 2000, 2700), \"DsgM\")\n",
    "\n",
    "c1 = ROOT.TCanvas()\n",
    "lgd = ROOT.TLegend(0.6, 0.6, 0.8, 0.8)\n",
    "lgd.AddEntry(MC_Dsg_mass_win_histo.GetPtr(), \"MC\", \"L\")\n",
    "lgd.AddEntry(Data_Dsg_mass_win_histo.GetPtr(), \"Data\", \"L\")\n",
    "MC_Dsg_mass_win_histo.SetLineColor(2)\n",
    "Data_Dsg_mass_win_histo.SetLineColor(1)\n",
    "Data_Dsg_mass_win_histo.Draw(\"E1 same\")\n",
    "MC_Dsg_mass_win_histo.Draw(\"E1 same\")\n",
    "lgd.Draw()\n",
    "c1.Draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Onde comprobamos que os novos `RDataFrames` tiveron unha reducción evidente no seu número de candidatos. Basicamente fixemos un corte para poder só ter os candidatos cuxa masa desconvolucionada sexa inferior a 2400 MeV/$c^2$. O tema está en que o corte é nunha variable pero afecta a todo o set de datos, de ahí que falemos dunha selección.\n",
    "\n",
    "O bo deste método é que é stackeable, é decir, ahora podemos engadir outro corte sobre o `RDataFrame`  que xa filtramos antes. Por exemplo, podemos facer os seguinte corte para o MonteCarlo\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tdf_MC_wind_ID = tdf_MC_wind.Filter(\"abs(gamma_MC_MOTHER_ID) == 433\")\n",
    "MC_Dsg_mass_win_ID_histo = tdf_MC_wind_ID.Histo1D((\"\", \"\", 100, 2000, 2400), \"DsgM\")\n",
    "\n",
    "c1 = ROOT.TCanvas()\n",
    "MC_Dsg_mass_win_ID_histo.GetXaxis().SetTitle(\"Dsg mass\")\n",
    "MC_Dsg_mass_win_ID_histo.GetYaxis().SetTitle(\"Nentries\")\n",
    "MC_Dsg_mass_win_ID_histo.Draw(\"E1\")\n",
    "c1.Draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "O que fixemos con este corte é, sobre o set de datos simulados que tiña $M(D_s^+\\gamma) < 2400 \\text{ } \\mathrm{MeV/c^2}$, quedarse só cos candidatos que teñan `gamma_MC_MOTHER_ID` igual a 433 en valor absoluto. Esta variable extraña forma parte dunha serie de variables especiais do MC que as solemos charmar _TRUEIDs_.\n",
    "\n",
    " Basicamente son unha serie de números que designan de onde provén unha particula dada. Neste caso temos unha simulación $D_s^{*+}\\to D_s^+\\gamma$, pero non todos os $\\gamma$ teñen como nai o $D_s^{*+}$. So o farán aqueles cuxa `gamma_MC_MOTHER_ID` sexa igual que o número que designa o $D_s^{*}$, que neste caso é $\\pm 433$ (+ para $D_s^{*+}$ e - $D_s^{*-}$). Entón, esixindo a condición anterior creamos un `RDataFrame` que ten só os candidatos da nosa sinal (bueno casi, faltaría poñer as TRUEIDs de $K^+ K^- \\pi^+$, que sería esixir que as súa nai sexa o $D_s^\\pm$ e a súa avoa o $D_s^{*\\pm}$). \n",
    " \n",
    " Este proceso de reconstruir no MC a desintegración mediante as TRUEIDs chámase *MCMatching* e é a forma de obter un set de datos de sinal puro. Este proceso é algo rutinario en todas as análisis de LHCb, polo que xa a introducimos aquí."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Por último, en vez de definir novos `RDataFrames` cada vez que fagamos un filtro, podemos simplemente aplicar sucesivamente o método `Filter` co método `Histo1D` para obter así directamente o histograma filtrado"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Data_Dsg_mass_win_sel_histo = tdf_data_wind.Filter(\"gamma_PT > 600 && gamma_CL > 0.7\").Histo1D((\"\", \"\", 100, 2000, 2400), \"DsgM\")\n",
    "Data_Dsg_mass_win_no_sel_histo = tdf_data_wind.Histo1D((\"\", \"\", 100, 2000, 2400), \"DsgM\")\n",
    "\n",
    "c1 = ROOT.TCanvas()\n",
    "lgd = ROOT.TLegend(0.6, 0.6, 0.8, 0.8)\n",
    "lgd.AddEntry(Data_Dsg_mass_win_no_sel_histo.GetPtr(), \"Raw\", \"l\")\n",
    "lgd.AddEntry(Data_Dsg_mass_win_sel_histo.GetPtr(), \"Sel\", \"l\")\n",
    "Data_Dsg_mass_win_no_sel_histo.SetLineColor(1)\n",
    "Data_Dsg_mass_win_sel_histo.SetLineColor(4)\n",
    "Data_Dsg_mass_win_sel_histo.DrawNormalized(\"same\")\n",
    "Data_Dsg_mass_win_no_sel_histo.DrawNormalized(\"same\")\n",
    "lgd.Draw()\n",
    "c1.Draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "O que fixemos foi facer un histograma da masa para os candidatos que teñan $p_{\\mathrm{T}}$ do fotón maior que 600 MeV/c __e__ CL do fotón maior que 0.7 (O _confidence level_ CL para o fotón é como o PID. Máis cerca de 1, máis probable é que sexa un fotón). Ó normalizar os plots vemos que o pico ca selección vése máis grande que o sin a selección. Iso pasa porque ca selección, a pesar de que quitamos parte dos candidatos do pico (sinal), estamos quitando bastantes máis de fondo (todo o que non é pico). Diseñar estas seleccións é un arte xa que hai que escoller as variables boas para cortar e canto cortar. Pero, polo menos, con `RDataFrame` isto faise máis ameno"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Apéndice: como funcionan as condicións lóxicas en códigos de C++ e python\n",
    "\n",
    "Para facer filtros, tanto con `RDataFrame.Filter()` e `pandas.DataFrame.query()` temos que introducir operacións lóxicas con cada unha das columnas que conforman o dataframe. Estas operacións lóxicas ríxense mediante os operadores `>, <, ==, <=, >=, !=`. Por exemplo, se queremos candidatos onde a enerxía do fotón é maior que a do $K^+$ poñeríamos\n",
    "\n",
    "```python\n",
    "tdf.Filter(\"gamma_PE > Kpl_PE\")\n",
    "```\n",
    "\n",
    "Por outra banda, se queremos aplicar máis dunha condición ó mesmo tempo temos os operadores AND `&` e OR `|`. Por exemplo, se queremos os candidatos de sinal no MC con $p_{\\mathrm{T}}$ maior que 600 MeV faríamos\n",
    "\n",
    "```python\n",
    "tdf_MC.Filter(\"gamma_MC_MOTHER_ID == 433 & gamma_PT > 600\")\n",
    "```\n",
    "\n",
    "E se quixéramos candidatos onde o $p_{\\mathrm{T}}$ do fotón sexa maior que 600 MeV __ou__ que $p_{\\mathrm{T}}$ do $\\pi^+$ sexa menor que 1500 MeV teríamos que facer\n",
    "\n",
    "```python\n",
    "tdf_data.Filter(\"gamma_PT > 600 | pi_PT < 1500\")\n",
    "```\n",
    "\n",
    "Estas condicións son stackeables, é decir, podemos poñer moitas de seguido\n",
    "\n",
    "```python\n",
    "tdf.Filter(\"condición 1 & condición 2 & condición 3 & ...\")\n",
    "```\n",
    "\n",
    "e tamén podemos mezclar operadores AND e OR\n",
    "\n",
    "```python\n",
    "tdf.Filter(\"condición 1 & condición 2 | condición 3 & ...\")\n",
    "```\n",
    "\n",
    "Finalmente, se queremos darlle un orde específico as condicións, podemos usar parénteses `()`\n",
    "\n",
    "```python\n",
    "tdf.Filter(\"(condición 1) & (condición 2 | condición 3)\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compatibilidades con python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Por último, `RDataFrame` como é unha librería moi reciente ten implementacións con python muy útiles. Por exemplo, podemos convertir o noso `RDataFrame` a `numpy` mediante o seguinte método."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np_tdf_data = tdf_data.AsNumpy()\n",
    "\n",
    "np_tdf_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Como comprobamos, este proceso é bastante lento, debido a que o `RDataFrame` ten 600 columnas e $2\\cdot 10^5$ filas. Ante isto, este método permítenos poñer só as columnas que queiramos"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np_tdf_data_2 = tdf_data.AsNumpy(columns=[\"DsgM\", \"gamma_PT\", \"Ds_M\", \"nVeloTracks\"])\n",
    "\n",
    "np_tdf_data_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Volvéndose moito máis rápido. Como comprobamos, o aplicar o método sobre o `RDataFrame` este devólvenos un diccionario de python cuxas *keys* son os nomes das columnas do `RDataFrame` e os seus elementos son `numpy.array`. Esta estructura é perfecta, xa que nos permite pasar directamente a `pandas.DataFrame`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df_data_2 = pd.DataFrame(np_tdf_data_2)\n",
    "\n",
    "df_data_2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(df_data_2[\"Ds_M\"], bins=100, histtype=\"step\", color='black')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Así, se non estamos interesados en usar nada de ROOT e centrarnos só en python, podemos usar `RDataFrame` para leer os .root files e logo pasalos a python con estes métodos. Eso sí, é necesario seleccionar so un número reducido de columnas para optimizar o tempo.\n",
    "\n",
    "Por último, se queremos volver a `RDataFrame` a partir dun `pandas.DataFrame` temos que facer o seguinte."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "columns = df_data_2.columns\n",
    "np_aux_2 = {key: df_data_2[key].values for key in columns}\n",
    "\n",
    "np_aux_2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tdf_from_numpy = ROOT.RDF.FromNumpy(np_aux_2)\n",
    "\n",
    "tdf_from_numpy.Display().Print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Onde comprobamos que recuperamos o `RDataFrame` sin ningún cambio. O que fixemos foi pasar o `pandas.DataFrame` a un diccionario. Para iso usamos esta liña\n",
    "\n",
    "```python\n",
    "np_aux_2 = {key: df_data_2[key].values for key in columns}\n",
    "```\n",
    "\n",
    "Que se coñece como un _dictionary compression_ ou algo así. Esto é un pouco avanzado xa que é unha técnica de python para facer bucles implícitos. Non nos centraremos en explicar isto pero sí que imos indicar porque o facemos asi. Cando pasamos de `RDataFrame` a `numpy` obtivemos un diccionario onde os seus elementos son `numpy.arrays`. Pois para facer o camiño inverso de `numpy` a `RDataFrame` coa función \n",
    "\n",
    "```python\n",
    "ROOT.RDF.FromNumpy()\n",
    "```\n",
    "\n",
    "É exactamente igual. Necesitamos darlle un obxeto homónimo. Se lle damos directamente o `pandas.DataFrame` non o vai a entender, de ahí que fagamos ese truco. Así, o mellor é crear unha función:\n",
    "\n",
    "\n",
    "```python\n",
    "def pandas_to_RDF(pd):\n",
    "    columns = pd.columns\n",
    "    np_pd = {key: pd[key].values for key in columns}\n",
    "    tdf = ROOT.RDF.FromNumpy(np_pd)\n",
    "    return tdf\n",
    "```\n",
    "\n",
    "Para facer este procedemento máis automático."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

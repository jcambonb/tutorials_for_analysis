# Tutorials for Analysis

 By Ivan Cambon Bouzas

 Instituto Galego de Física de Altas Enerxías (IGFAE)
 
 Universidade de Santiago de Compostela (USC)


# Introdución

Neste repositorio inclúese unha serie de tutoriais para poder aprender a usar ROOT en Python. En cada un explicarase como se utiliza cada unha das clases características de ROOT mediante exemplos visuais. O tutoriais estarán focalizados a utilizar:

- Clases basicas de ROOT: TGraph, TH1, TH2, TCanvas, TTree, etc.
- RDataFrame
- RooFit

Que son as clases máis utilizadas nas análise de datos que se propoñen para facer TFGs/TFMs. Os tutoriais estarán en galego, pero tamén pódense considerar versións en ingles ou castelán. Por último, o recorrido básico recomendado dos tutoriais é o seguinte

- ROOT_basics_1
- ROOT_basics_2
- ROOT_basics_3
- RDataFrame_basics
- RooFit_basics

RooFit_advanced queda como algo opcional. Pode ser que a futuro creese un de TMVA, pero hai que comprobalo.

# Requerimentos

Para poder correr estos códigos é necesario ter unha instalación de python que teña ROOT instalado. Recomendamos facelo mediante un entorno de conda. Por outra banda, nalgúns tutoriais usaránse arquivos de ROOT específicos da análise de Iván Cambón Bouzas. Este atópanse nun disco dos nodos do IGFAE, polo que é recomendable correr estos tutoriais nos nodos mediante o entorno conda que temos creado no grupo de LHCb de Santiago.


